\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Continuous Integration}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Purpose of Continuous Integration}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Knowing the State of the Project}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Reducing Number of Bugs}{5}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Advantages of Continuous Integration}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Disadvantages of Continuous Integration}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Continuous Integration Practices}{6}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Agreement in the Team}{6}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Version Control}{7}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Code Inspection}{7}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Build Automation}{7}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Automated Tests and Self-testing}{7}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}Testing in Environment Simulating Production}{8}{subsection.2.4.6}
\contentsline {subsection}{\numberline {2.4.7}Automated Deployment}{8}{subsection.2.4.7}
\contentsline {section}{\numberline {2.5}Continuous Integration Tools}{8}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Comparison of Continuous Integration Tools}{9}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Tools Used in this Thesis}{11}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Jenkins}{11}{subsection.2.6.1}
\contentsline {subsubsection}{Short History of Jenkins}{11}{section*.2}
\contentsline {subsubsection}{Key Features of Jenkins Tool}{11}{section*.3}
\contentsline {subsection}{\numberline {2.6.2}Foreman}{12}{subsection.2.6.2}
\contentsline {chapter}{\numberline {3}Build Automation}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}Build Automation Tools}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Make Utility}{14}{subsection.3.1.1}
\contentsline {subsubsection}{Summary \discretionary {-}{}{}-\ features}{14}{section*.4}
\contentsline {subsection}{\numberline {3.1.2}Ant}{15}{subsection.3.1.2}
\contentsline {subsubsection}{Summary \discretionary {-}{}{}-\ features}{15}{section*.5}
\contentsline {subsection}{\numberline {3.1.3}Maven}{15}{subsection.3.1.3}
\contentsline {subsubsection}{Summary \discretionary {-}{}{}-\ features}{15}{section*.6}
\contentsline {subsection}{\numberline {3.1.4}Gradle}{16}{subsection.3.1.4}
\contentsline {subsubsection}{Summary \discretionary {-}{}{}-\ features}{16}{section*.7}
\contentsline {subsection}{\numberline {3.1.5}Summary}{16}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Analysis of Real Examples of the Build Automation using Continuous Integration}{16}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Mozilla and Build Automation}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Docker and Build Automation}{17}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Build Automation and Continuous Integration For Developers}{18}{subsection.3.2.3}
\contentsline {subsubsection}{Use Case 1: Python Script}{18}{section*.8}
\contentsline {subsubsection}{Use Case 2: NodeJS Server Development}{18}{section*.9}
\contentsline {subsubsection}{Use Case 3: Apache Configuration}{19}{section*.10}
\contentsline {subsubsection}{Use Case 4: HTML, JavaScript and CSS Development}{19}{section*.11}
\contentsline {subsubsection}{Use Case 5: PHP Development}{19}{section*.12}
\contentsline {section}{\numberline {3.3}Summary}{20}{section.3.3}
\contentsline {chapter}{\numberline {4}System Using Build Automation and Continuous Integration Methodics}{21}{chapter.4}
\contentsline {section}{\numberline {4.1}Description of the System}{21}{section.4.1}
\contentsline {section}{\numberline {4.2}Components of the System}{21}{section.4.2}
\contentsline {section}{\numberline {4.3}Description of Behaviour}{22}{section.4.3}
\contentsline {section}{\numberline {4.4}Description of the Implementation}{23}{section.4.4}
\contentsline {section}{\numberline {4.5}Evaluation of the System}{23}{section.4.5}
\contentsline {chapter}{\numberline {5}Conclusion}{24}{chapter.5}
\contentsline {section}{\numberline {5.1}Future development}{24}{section.5.1}
\contentsline {chapter}{Bibliography}{26}{chapter*.13}
\contentsline {chapter}{Appendices}{28}{section*.14}
\contentsline {section}{List of Appendices}{29}{section*.15}
\contentsline {chapter}{\numberline {A}Content of DVD}{30}{appendix.A}
\contentsline {chapter}{\numberline {B}Manual}{31}{appendix.B}
\contentsline {section}{\numberline {B.1}Installing VirtualBox}{31}{section.B.1}
\contentsline {section}{\numberline {B.2}Configure VirtualBox}{31}{section.B.2}
\contentsline {section}{\numberline {B.3}Reassembling VirtualBox file}{31}{section.B.3}
\contentsline {section}{\numberline {B.4}Importing virtual machines from VirtualBox import file}{32}{section.B.4}
\contentsline {section}{\numberline {B.5}Summary}{32}{section.B.5}
\contentsline {section}{\numberline {B.6}User manual}{32}{section.B.6}
\contentsfinish 
